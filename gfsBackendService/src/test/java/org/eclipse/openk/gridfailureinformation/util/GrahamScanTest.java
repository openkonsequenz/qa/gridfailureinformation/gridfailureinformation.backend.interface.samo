/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.gridfailureinformation.util;

import org.junit.jupiter.api.Test;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import static org.eclipse.openk.gridfailureinformation.util.GrahamScan.getConvexHull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class GrahamScanTest {
    @Test
    public void shouldReturnFivePoints(){
        List<Point> points = new ArrayList<>() ;

        points.add(new Point(1,1));
        points.add(new Point(1,5));
        points.add(new Point(2,2));
        points.add(new Point(2,4));
        points.add(new Point(4,3));
        points.add(new Point(5,5));
        points.add(new Point(5,1));

        List<Point> polygonPoints = getConvexHull(points) ;

        assertEquals(5, polygonPoints.size());
    }

    @Test
    public void shouldReturnEmptyList_collinearPoints(){
        List<Point> points = new ArrayList<>() ;

        points.add(new Point(1,5));
        points.add(new Point(1,2));
        points.add(new Point(1,4));

        List<Point> polygonPoints = getConvexHull(points) ;

        assertTrue(polygonPoints.isEmpty());
    }

    @Test
    public void shouldReturnEmptyList_toFewPoints(){
        List<Point> points = new ArrayList<>() ;

        points.add(new Point(1,2));
        points.add(new Point(1,4));

        List<Point> polygonPoints = getConvexHull(points) ;

        assertTrue(polygonPoints.isEmpty());
    }
}
