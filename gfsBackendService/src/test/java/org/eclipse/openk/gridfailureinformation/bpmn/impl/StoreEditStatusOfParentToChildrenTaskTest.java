/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.bpmn.impl;

import org.eclipse.openk.gridfailureinformation.GridFailureInformationApplication;
import org.eclipse.openk.gridfailureinformation.bpmn.base.ProcessException;
import org.eclipse.openk.gridfailureinformation.bpmn.base.ProcessTask;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.tasks.ProcessHelper;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.tasks.StoreEditStatusOfParentToChildrenTask;
import org.eclipse.openk.gridfailureinformation.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationDto;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.util.List;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = GridFailureInformationApplication.class)
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
class StoreEditStatusOfParentToChildrenTaskTest {
    @Test
    void shouldDecideYes() throws ProcessException {
        ProcessHelper processHelper = mock(ProcessHelper.class);
        List<FailureInformationDto> children = MockDataHelper.mockGridFailureInformationDtos();
        when( processHelper.getSubordinatedChildren( any(FailureInformationDto.class)))
                .thenReturn(children);
        when( processHelper.getProcessStateFromStatusUuid(any(UUID.class))).thenReturn(GfiProcessState.UPDATED);

        GfiProcessSubject sub = GfiProcessSubject.of(MockDataHelper.mockFailureInformationDto(), processHelper);

        ProcessTask daTask = new StoreEditStatusOfParentToChildrenTask();
        daTask.leaveStep(sub);

        verify( processHelper, times( children.size()))
                .storeEditStatus(any(FailureInformationDto.class), eq(GfiProcessState.UPDATED));
    }
}
