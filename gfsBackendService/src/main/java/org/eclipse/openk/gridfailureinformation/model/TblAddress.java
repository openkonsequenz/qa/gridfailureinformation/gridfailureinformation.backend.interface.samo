/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.gridfailureinformation.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.Data;

import java.math.BigDecimal;
import java.util.UUID;

@Data
@Entity
@Table(name = "TBL_GFI_ADDRESS")
public class TblAddress {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,  generator = "TBL_GFI_ADDRESS_ID_SEQ")
    @SequenceGenerator(name = "TBL_GFI_ADDRESS_ID_SEQ", sequenceName = "TBL_GFI_ADDRESS_ID_SEQ", allocationSize = 1)
    @Column(name = "id", updatable = false)
    private Long id;
    private UUID uuid;
    private BigDecimal sdox1;
    private BigDecimal sdoy1;
    private Long g3efid;
    private String postcode;
    private String community;
    private String district;
    private String street;
    private String housenumber;
    private boolean waterConnection;
    private String waterGroup;
    private boolean gasConnection;
    private String gasGroup;
    private boolean powerConnection;
    private boolean districtheatingConnection;
    private boolean telecommConnection;
    private String stationId;
    private BigDecimal longitude;
    private BigDecimal latitude;

}
