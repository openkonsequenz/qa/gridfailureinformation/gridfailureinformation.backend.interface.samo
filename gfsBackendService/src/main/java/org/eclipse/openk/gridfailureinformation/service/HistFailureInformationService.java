/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.gridfailureinformation.service;


import org.eclipse.openk.gridfailureinformation.exceptions.NotFoundException;
import org.eclipse.openk.gridfailureinformation.mapper.HistFailureInformationMapper;
import org.eclipse.openk.gridfailureinformation.model.HtblFailureInformation;
import org.eclipse.openk.gridfailureinformation.repository.BranchRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureClassificationRepository;
import org.eclipse.openk.gridfailureinformation.repository.HistFailureInformationRepository;
import org.eclipse.openk.gridfailureinformation.repository.StatusRepository;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.StationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;


@Service
public class HistFailureInformationService {
    private final HistFailureInformationRepository histFailureInformationRepository;

    private final HistFailureInformationMapper histFailureInformationMapper;

    private final HistFailureInformationStationService histFailureInformationStationService;

    public HistFailureInformationService(HistFailureInformationRepository histFailureInformationRepository, HistFailureInformationMapper histFailureInformationMapper, HistFailureInformationStationService histFailureInformationStationService) {
        this.histFailureInformationRepository = histFailureInformationRepository;
        this.histFailureInformationMapper = histFailureInformationMapper;
        this.histFailureInformationStationService = histFailureInformationStationService;
    }

    public List<FailureInformationDto> getFailureInformationVersionsByUuid(UUID uuid) {
        List<HtblFailureInformation> htblFailureInformationList = histFailureInformationRepository.findByUuid(uuid);

        List<FailureInformationDto> failureInfoDtoList = htblFailureInformationList
                .stream()
                .map(histFailureInformationMapper::toFailureInformationDto)
                .sorted( (x, y) -> x.getVersionNumber().compareTo(y.getVersionNumber()))
                .collect(Collectors.toList());

        for (FailureInformationDto failureInfoDto : failureInfoDtoList) {
            addStationsToFailureInfoDto(failureInfoDto);
        }
        return failureInfoDtoList;

    }

    public FailureInformationDto getFailureInformationVersion(UUID uuid, Long versionNumber ) {
        List<HtblFailureInformation> htblFailureInformation = histFailureInformationRepository.findByUuidAndVersionNumber(uuid, versionNumber);

        if (htblFailureInformation.isEmpty()) {
            throw new NotFoundException();
        }

        FailureInformationDto histFailureInfoDto = histFailureInformationMapper.toFailureInformationDto(htblFailureInformation.get(0));

        return addStationsToFailureInfoDto(histFailureInfoDto);
    }

    private FailureInformationDto addStationsToFailureInfoDto (FailureInformationDto histFailureInfoDto) {

        List<StationDto> stationDtoList = histFailureInformationStationService.findHistStationsByFailureInfoAndVersionNumber(histFailureInfoDto.getUuid(), histFailureInfoDto.getVersionNumber());
        List<UUID> stationUuidList = stationDtoList.stream()
                .map(StationDto::getUuid)
                .collect(Collectors.toList());

        histFailureInfoDto.setStationIds(stationUuidList);

        return histFailureInfoDto;

    }

}
