/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.gridfailureinformation.bpmn.impl.tasks;

import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.bpmn.base.ProcessException;
import org.eclipse.openk.gridfailureinformation.bpmn.base.tasks.DecisionTask;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.GfiProcessSubject;

@Log4j2
public class DecideFailureInfoPlanned extends DecisionTask<GfiProcessSubject> {
    public DecideFailureInfoPlanned() {
        super("Decision: Ist die Störungsinformation geplant?");
    }

    @Override
    public OutputPort decide(GfiProcessSubject subject) throws ProcessException {

        boolean isFailureInfoPlanned = subject.getProcessHelper().getEnvironment().getFailureInformationService().isFailureInfoPlanned(
                subject.getFailureInformationDto());

        String loggerOutput1 = "Decide: ";

        if( isFailureInfoPlanned ) {
            log.debug(loggerOutput1 + getDescription()+"\" -> Firing YES");
            return OutputPort.YES;
        }
        else {
            log.debug(loggerOutput1 + getDescription()+"\" -> Firing NO");
            return OutputPort.NO;
        }
    }

}
