/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.gridfailureinformation.bpmn.impl.tasks;

import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.bpmn.base.ProcessException;
import org.eclipse.openk.gridfailureinformation.bpmn.base.ProcessState;
import org.eclipse.openk.gridfailureinformation.bpmn.base.tasks.UserInteractionTask;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.GfiProcessState;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.GfiProcessSubject;
import org.eclipse.openk.gridfailureinformation.constants.Constants;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationDto;

@Log4j2
public class UIStoreFailureInformationTask extends UserInteractionTask<GfiProcessSubject> {
    private final boolean stayIfStateUnchanged;

    public UIStoreFailureInformationTask(String description, boolean stayIfStateUnchanged) {
        super(description);
        this.stayIfStateUnchanged = stayIfStateUnchanged;
    }

    @Override
    protected void onLeaveStep(GfiProcessSubject subject) throws ProcessException {
        subject.setFailureInformationDto(  // store and refresh the dto in the subject
            subject.getProcessHelper().storeFailureFromViewModel(subject.getFailureInformationDto())
        );

        // send storniert emails
        if ("storniert".equals(subject.getFailureInformationDto().getStatusIntern()) && Constants.PUB_STATUS_VEROEFFENTLICHT.equalsIgnoreCase(subject.getFailureInformationDto().getPublicationStatus())){
            subject.getProcessHelper().publishFailureInformation(subject.getFailureInformationDto(), true , GfiProcessState.CANCELED);
        }
    }

    @Override
    protected void onEnterStep(GfiProcessSubject subject) {
        // nothing to do here
    }

    @Override
    protected boolean isStayInThisTask(GfiProcessSubject subject){
        return stayIfStateUnchanged && !detectStateChanged(subject);
    }

    @Override
    protected void onStayInTask(GfiProcessSubject subject) throws ProcessException {
        FailureInformationDto failureInformationDto = subject.getProcessHelper().storeFailureFromViewModel(subject.getFailureInformationDto());
        subject.setFailureInformationDto(failureInformationDto); // store and refresh the dto in the subject
    }

    private boolean detectStateChanged(GfiProcessSubject subject) {
        ProcessState stateFromObj = subject.getProcessHelper().getProcessStateFromStatusUuid(
                subject.getFailureInformationDto().getStatusInternId());
        return stateFromObj != subject.getStateInDb();
    }
}
